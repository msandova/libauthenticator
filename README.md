# Libauthenticator

![Crates.io](https://img.shields.io/crates/l/libauthenticator)
[![Crates.io](https://img.shields.io/crates/v/libauthenticator)][crate]
[![Docs.rs](https://docs.rs/libauthenticator/badge.svg)][API DOCS]

Simple crate to compute HOTP and TOTP tokens.

[crate]:https://crates.io/crates/libauthenticator
[API DOCS]:https://docs.rs/libauthenticator
