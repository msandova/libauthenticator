use crate::{Algorithm, HOTP, OTP};
use anyhow::Result;

pub static TOTP_DEFAULT_PERIOD: u32 = 30;

pub struct TOTP {
    pub algorithm: Algorithm,
    pub digits: u32,
    pub period: u32,
    pub secret: String,
}

impl OTP for TOTP {
    /// Performs the [HMAC-based One-time Password Algorithm](http://en.wikipedia.org/wiki/HMAC-based_One-time_Password_Algorithm)
    /// (HOTP) given an RFC4648 base32 encoded secret, and an integer counter.
    fn compute(&self, counter: u64) -> Result<String> {
        let hotp = HOTP::from(self);
        hotp.compute(counter)
    }
}
