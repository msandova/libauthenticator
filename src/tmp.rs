pub struct HOTP {
    algorithm: Algorithm,
    digits: u32,
    secret: &'static str,
}

impl HOTP {
    /// Decodes a secret (given as an RFC4648 base32-encoded ASCII string)
    /// into a byte string. It fails if secret is not a valid Base32 string.
    fn decode_secret(&self) -> Result<Vec<u8>> {
        let res = BASE32.decode(self.secret.as_bytes())?;
        Ok(res)
    }

    /// Validates if `secret` is a valid Base32 String.
    pub fn is_valid(&self) -> bool {
        decode_secret(self.secret).is_ok()
    }

    /// Calculates the HMAC digest for the given secret and counter.
    fn calc_digest(&self, decoded_secret: &[u8], counter: u64) -> hmac::Tag {
        let key = hmac::Key::new(self.algorithm.into(), decoded_secret);
        hmac::sign(&key, &counter.to_be_bytes())
    }

    /// Encodes the HMAC digest into a n-digit integer.
    fn encode_digest(digest: &[u8]) -> Result<u32> {
        let offset = match digest.last() {
            Some(x) => *x & 0xf,
            None => anyhow::bail!("Invalid digest"),
        } as usize;
        let code_bytes: [u8; 4] = match digest[offset..offset + 4].try_into() {
            Ok(x) => x,
            Err(_) => anyhow::bail!("Invalid digest"),
        };
        let code = u32::from_be_bytes(code_bytes);
        Ok(code & 0x7fffffff)
    }

    /// Performs the [HMAC-based One-time Password Algorithm](http://en.wikipedia.org/wiki/HMAC-based_One-time_Password_Algorithm)
    /// (HOTP) given an RFC4648 base32 encoded secret, and an integer counter.
    pub fn hotp(&self, counter: u64) -> Result<u32> {
        let decoded = decode_secret(self.secret)?;
        let digest = encode_digest(calc_digest(decoded.as_slice(), counter, self.algorithm).as_ref())?;
        Ok(digest % 10_u32.pow(self.digits))
    }

    pub fn steam(secret: &str, counter: u64) -> Result<String> {
        let decoded = decode_secret(secret)?;
        let mut full_token =
            encode_digest(calc_digest(decoded.as_slice(), counter, Algorithm::SHA1).as_ref())?;

        let mut code = String::new();
        let total_chars = STEAM_CHARS.len() as u32;
        for _ in 0..STEAM_DEFAULT_DIGITS {
            let pos = full_token % total_chars;
            let charachter = STEAM_CHARS.chars().nth(pos as usize).unwrap();
            code.push(charachter);
            full_token /= total_chars;
        }
        Ok(code)
    }

    pub fn format(code: u32, digits: usize) -> String {
        let padded_code = format!("{:0width$}", code, width = digits);
        let mut formated_code = String::new();
        for (idx, ch) in padded_code.chars().enumerate() {
            if (digits - idx) % 3 == 0 && (digits - idx) != 0 && idx != 0 {
                formated_code.push(' ');
            }
            formated_code.push(ch);
        }
        formated_code
    }

    pub fn time_based_counter(period: u32) -> u64 {
        let timestamp = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_secs();
        timestamp / period as u64
    }

    pub fn remaining_time(&self) -> Duration {
        let period = self.period as u128 * 1000;
        let now: u128 = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_millis();
        let duration = period - now % period;
        Duration::from_millis(duration as u64)
    }
}
