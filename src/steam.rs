use crate::{calc_digest, decode_secret, encode_digest, Algorithm, OTP};
use anyhow::Result;

static STEAM_CHARS: &str = "23456789BCDFGHJKMNPQRTVWXY";
pub static STEAM_DEFAULT_PERIOD: u32 = 30;
pub static STEAM_DEFAULT_DIGITS: u32 = 5;

pub struct Steam {
    pub secret: String,
}

impl OTP for Steam {
    fn compute(&self, counter: u64) -> Result<String> {
        let decoded = decode_secret(&self.secret)?;
        let mut full_token =
            encode_digest(calc_digest(decoded.as_slice(), counter, Algorithm::SHA1).as_ref())?;

        let mut code = String::new();
        let total_chars = STEAM_CHARS.len() as u32;
        for _ in 0..STEAM_DEFAULT_DIGITS {
            let pos = full_token % total_chars;
            let charachter = STEAM_CHARS.chars().nth(pos as usize).unwrap();
            code.push(charachter);
            full_token /= total_chars;
        }
        Ok(code)
    }
}
