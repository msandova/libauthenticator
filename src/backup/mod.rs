use crate::{Algorithm, OTPMethod, Steam, HOTP, OTP, TOTP};
use anyhow::Result;

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Account {
    pub secret: String,
    pub digits: u32,
    pub period: Option<u32>,
    pub counter: Option<u32>,

    pub method: OTPMethod,
    pub algorithm: Algorithm,
    pub issuer: Option<String>,
    pub label: String,
}

pub trait Restorable: Sized {
    fn restore(bytes: &[u8]) -> Result<Vec<Account>>;
}

pub trait Backupable: Sized {
    fn backup(codes: Vec<Account>) -> Result<Vec<u8>>;
}

impl OTP for Account {
    fn compute(&self, counter: u64) -> Result<String> {
        match self.method {
            OTPMethod::TOTP => TOTP {
                algorithm: self.algorithm,
                digits: self.digits,
                period: self.period.unwrap(),
                secret: self.secret.clone(),
            }
            .compute(counter),
            OTPMethod::HOTP => HOTP {
                algorithm: self.algorithm,
                digits: self.digits,
                secret: self.secret.clone(),
            }
            .compute(counter),
            OTPMethod::Steam => Steam {
                secret: self.secret.clone(),
            }
            .compute(counter),
        }
    }
}

mod andotp;
pub use andotp::AndOTP;
// mod freeotp;
// mod legacy;
// pub use self::{andotp::AndOTP, freeotp::FreeOTP, legacy::LegacyAuthenticator};
