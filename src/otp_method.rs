use serde::{Deserialize, Serialize};
use std::{fmt, str::FromStr};

#[derive(Debug, Eq, PartialEq, Clone, Copy, Serialize, Deserialize)]
pub enum OTPMethod {
    HOTP,
    TOTP,
    Steam,
}

impl fmt::Display for OTPMethod {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match *self {
            OTPMethod::TOTP => "totp",
            OTPMethod::HOTP => "hotp",
            OTPMethod::Steam => "steam",
        };
        write!(f, "{}", s)
    }
}

impl Default for OTPMethod {
    fn default() -> Self {
        Self::TOTP
    }
}

impl FromStr for OTPMethod {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_ref() {
            "totp" | "otp" => Ok(Self::TOTP),
            "hotp" => Ok(Self::HOTP),
            "steam" => Ok(Self::Steam),
            _ => anyhow::bail!("Unsupported OTPMethod"),
        }
    }
}

// impl<T: OTP> From<T> for OTPMethod {
//     fn from(self) -> Self {
//         match self {
//             HOTP {..} => OTPMethod::HOTP,
//             TOTP {..} => OTPMethod::TOTP,
//             Steam {..} => OTPMethod::Steam,
//             _ => unimplemented!(),
//         }
//     }
// }
