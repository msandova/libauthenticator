#[cfg(test)]
mod tests {
    use libauthenticator::backup::Restorable;
    use libauthenticator::{
        backup::{AndOTP, Backupable},
        format, Account, Algorithm, OTPMethod, Steam, DEFAULT_DIGITS, HOTP, OTP,
    };

    #[test]
    fn test_hotp() {
        let hotp = HOTP {
            algorithm: Algorithm::SHA1,
            digits: DEFAULT_DIGITS,
            secret: "BASE32SECRET3232".to_string(),
        };
        assert_eq!(hotp.compute(0).unwrap(), "260182");
        assert_eq!(hotp.compute(1).unwrap(), "055283");
        assert_eq!(hotp.compute(1401).unwrap(), "316439");
    }

    #[test]
    fn test_padding_hotp() {
        let hotp = HOTP {
            algorithm: Algorithm::SHA1,
            digits: DEFAULT_DIGITS,
            secret: "BASE32SECRET====".to_string(),
        };
        assert_eq!(hotp.compute(0).unwrap(), "157902");
        assert_eq!(hotp.compute(42).unwrap(), "087008");
    }

    #[test]
    fn test_steam_totp() {
        let steam = Steam {
            secret: "BASE32SECRET3232".to_string(),
        };
        assert_eq!(steam.compute(0).unwrap(), "2TC8B");
        assert_eq!(steam.compute(1).unwrap(), "YKKK4");
    }

    #[test]
    fn test_otp_format() {
        assert_eq!(format(01234, 5), "01 234");
        assert_eq!(format(01234, 6), "001 234");
        assert_eq!(format(123456, 6), "123 456");
        assert_eq!(format(01234, 7), "0 001 234");
        assert_eq!(format(01234567, 8), "01 234 567");
        assert_eq!(format(12345678, 8), "12 345 678");
    }

    #[test]
    fn andotp() {
        use std::fs::File;
        use std::io::prelude::*;

        let mut file = File::open("tests/andotp.json").unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents).unwrap();

        let bytes = contents.as_bytes();
        let accounts = AndOTP::restore(bytes).unwrap();

        let mail_acc = Account {
            secret: "BASE32SECRET32".into(),
            digits: 6,
            period: Some(30),
            counter: None,
            method: OTPMethod::TOTP,
            algorithm: Algorithm::SHA1,
            issuer: Some("Mail".into()),
            label: "foo@bar.com".into(),
        };
        let vcs_acc = Account {
            secret: "BASE32SECRET32-1".into(),
            digits: 6,
            period: Some(30),
            counter: None,
            method: OTPMethod::TOTP,
            algorithm: Algorithm::SHA1,
            issuer: Some("Mail".into()),
            label: "foo@bar.com".into(),
        };
        assert_eq!(accounts[0], mail_acc);
        assert_eq!(accounts.len(), 2);

        let accounts = vec![mail_acc, vcs_acc];
        let b = AndOTP::backup(accounts.clone()).unwrap();
        assert_eq!(AndOTP::restore(&b).unwrap(), accounts.clone());
    }
}
